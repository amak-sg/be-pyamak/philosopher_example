from enum import *
class State(Enum):
        """
        The scheduler is running
        """
        THINK = auto()

        """
        The scheduler is paused
        """
        HUNGRY = auto()

        """
        The scheduler is expected to stop at the end at the current cycle
        """
        EATING = auto()

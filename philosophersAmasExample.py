from philosophersExample import PhilosophersExample
from tableExample import TableExample
from pyAmakCore.classes.amas import Amas
from pyAmakCore.enumeration.scheduling import scheduling

class PhilosophersAmasExamples(Amas):

    def __init__(self):
        super().__init__(TableExample())
        self.__observer = None

    def get_Agents_Sorted(self):
        agents = self.get_agents()
        agents.sort(key=lambda x: x.get_id())
        return agents

    def on_initial_agents_creation(self):
        ps = []
        for i in range(9):
            ps.append(PhilosophersExample(i,self,self.get_environment().get_forks()[i],self.get_environment().get_forks()[i+1]))

        ps.append(PhilosophersExample(9,self,self.get_environment().get_forks()[9],self.get_environment().get_forks()[0]))

        for j in range(len(ps)-1):
            ps[j+1].add_neighbour(ps[j])
            ps[j].add_neighbour(ps[j+1])

        ps[0].add_neighbour(ps[len(ps)-1])
        ps[len(ps)-1].add_neighbour(ps[0])
        self.add_agents(ps)

    def attach(self, observer: 'Controleur') -> None:
        self.__observer = observer

    def notifyObserverAboutCycle(self) -> None:
        self.__observer.updateCycle(self.get_environment(), self)

    def cycle(self) -> None:
        super().cycle()
        self.notifyObserverAboutCycle()
